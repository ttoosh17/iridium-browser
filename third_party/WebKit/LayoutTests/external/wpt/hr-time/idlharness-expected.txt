This is a testharness.js-based test.
PASS Window interface: attribute performance 
PASS WorkerGlobalScope interface: existence and properties of interface object 
PASS Performance interface: existence and properties of interface object 
PASS Performance interface object length 
PASS Performance interface object name 
FAIL Performance interface: existence and properties of interface prototype object assert_equals: class string of Performance.prototype expected "[object PerformancePrototype]" but got "[object Performance]"
PASS Performance interface: existence and properties of interface prototype object's "constructor" property 
PASS Performance interface: operation now() 
PASS Performance must be primary interface of window.performance 
PASS Stringification of window.performance 
PASS Performance interface: window.performance must inherit property "now" with the proper type (0) 
Harness: the test ran to completion.

