This is a testharness.js-based test.
Found 59 tests; 58 PASS, 1 FAIL, 0 TIMEOUT, 0 NOTRUN.
PASS Selection interface: existence and properties of interface object 
PASS Selection interface object length 
PASS Selection interface object name 
FAIL Selection interface: existence and properties of interface prototype object assert_equals: class string of Selection.prototype expected "[object SelectionPrototype]" but got "[object Selection]"
PASS Selection interface: existence and properties of interface prototype object's "constructor" property 
PASS Selection interface: attribute anchorNode 
PASS Selection interface: attribute anchorOffset 
PASS Selection interface: attribute focusNode 
PASS Selection interface: attribute focusOffset 
PASS Selection interface: attribute isCollapsed 
PASS Selection interface: attribute rangeCount 
PASS Selection interface: attribute type 
PASS Selection interface: operation getRangeAt(unsigned long) 
PASS Selection interface: operation addRange(Range) 
PASS Selection interface: operation removeRange(Range) 
PASS Selection interface: operation removeAllRanges() 
PASS Selection interface: operation empty() 
PASS Selection interface: operation collapse(Node,unsigned long) 
PASS Selection interface: operation setPosition(Node,unsigned long) 
PASS Selection interface: operation collapseToStart() 
PASS Selection interface: operation collapseToEnd() 
PASS Selection interface: operation extend(Node,unsigned long) 
PASS Selection interface: operation setBaseAndExtent(Node,unsigned long,Node,unsigned long) 
PASS Selection interface: operation selectAllChildren(Node) 
PASS Selection interface: operation deleteFromDocument() 
PASS Selection interface: operation containsNode(Node,boolean) 
PASS Selection interface: stringifier 
PASS Selection must be primary interface of getSelection() 
PASS Stringification of getSelection() 
PASS Selection interface: getSelection() must inherit property "anchorNode" with the proper type (0) 
PASS Selection interface: getSelection() must inherit property "anchorOffset" with the proper type (1) 
PASS Selection interface: getSelection() must inherit property "focusNode" with the proper type (2) 
PASS Selection interface: getSelection() must inherit property "focusOffset" with the proper type (3) 
PASS Selection interface: getSelection() must inherit property "isCollapsed" with the proper type (4) 
PASS Selection interface: getSelection() must inherit property "rangeCount" with the proper type (5) 
PASS Selection interface: getSelection() must inherit property "type" with the proper type (6) 
PASS Selection interface: getSelection() must inherit property "getRangeAt" with the proper type (7) 
PASS Selection interface: calling getRangeAt(unsigned long) on getSelection() with too few arguments must throw TypeError 
PASS Selection interface: getSelection() must inherit property "addRange" with the proper type (8) 
PASS Selection interface: calling addRange(Range) on getSelection() with too few arguments must throw TypeError 
PASS Selection interface: getSelection() must inherit property "removeRange" with the proper type (9) 
PASS Selection interface: calling removeRange(Range) on getSelection() with too few arguments must throw TypeError 
PASS Selection interface: getSelection() must inherit property "removeAllRanges" with the proper type (10) 
PASS Selection interface: getSelection() must inherit property "empty" with the proper type (11) 
PASS Selection interface: getSelection() must inherit property "collapse" with the proper type (12) 
PASS Selection interface: calling collapse(Node,unsigned long) on getSelection() with too few arguments must throw TypeError 
PASS Selection interface: getSelection() must inherit property "setPosition" with the proper type (13) 
PASS Selection interface: calling setPosition(Node,unsigned long) on getSelection() with too few arguments must throw TypeError 
PASS Selection interface: getSelection() must inherit property "collapseToStart" with the proper type (14) 
PASS Selection interface: getSelection() must inherit property "collapseToEnd" with the proper type (15) 
PASS Selection interface: getSelection() must inherit property "extend" with the proper type (16) 
PASS Selection interface: calling extend(Node,unsigned long) on getSelection() with too few arguments must throw TypeError 
PASS Selection interface: getSelection() must inherit property "setBaseAndExtent" with the proper type (17) 
PASS Selection interface: calling setBaseAndExtent(Node,unsigned long,Node,unsigned long) on getSelection() with too few arguments must throw TypeError 
PASS Selection interface: getSelection() must inherit property "selectAllChildren" with the proper type (18) 
PASS Selection interface: calling selectAllChildren(Node) on getSelection() with too few arguments must throw TypeError 
PASS Selection interface: getSelection() must inherit property "deleteFromDocument" with the proper type (19) 
PASS Selection interface: getSelection() must inherit property "containsNode" with the proper type (20) 
PASS Selection interface: calling containsNode(Node,boolean) on getSelection() with too few arguments must throw TypeError 
Harness: the test ran to completion.

