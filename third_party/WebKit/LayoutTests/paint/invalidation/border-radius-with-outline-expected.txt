{
  "layers": [
    {
      "name": "LayoutView #document",
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow (positioned) DIV id='target'",
          "rect": [45, 45, 310, 310],
          "reason": "border box change"
        }
      ]
    }
  ],
  "objectPaintInvalidations": [
    {
      "object": "LayoutBlockFlow (positioned) DIV id='target'",
      "reason": "border box change"
    }
  ]
}

