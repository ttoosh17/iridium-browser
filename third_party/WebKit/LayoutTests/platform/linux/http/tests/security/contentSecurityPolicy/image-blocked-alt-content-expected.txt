layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x91
  LayoutBlockFlow {HTML} at (0,0) size 800x91
    LayoutBlockFlow {BODY} at (8,8) size 784x75
      LayoutText {#text} at (0,0) size 737x74
        text run at (0,0) width 737: "This test shows what alt content looks like when an image errors out on a page with a CSP that blocks data: URIs for"
        text run at (0,55) width 52: "images. "
      LayoutBlockFlow {IMG} at (52,20) size 50x50
      LayoutText {#text} at (0,0) size 0x0
layer at (60,28) size 50x50 clip at (61,29) size 48x48
  LayoutBlockFlow {DIV} at (0,0) size 50x50 [border: (1px solid #C0C0C0)]
    LayoutImage (floating) {IMG} at (2,2) size 16x16
layer at (78,30) size 30x40
  LayoutBlockFlow {DIV} at (18,2) size 30x40
    LayoutText {#text} at (0,0) size 23x39
      text run at (0,0) width 15: "alt"
      text run at (0,20) width 23: "text"
